const unique = (value, index, self) => {
	return self.indexOf(value) === index
  }

function distance(first, second){
	let dist = 0;
	if (Array.isArray(first) && Array.isArray(second))
	{
		if (first.length < 1 || second.length < 1)
	{
		dist = 0;
	}
	else
	{	let count = 1;
		first.sort();
		second.sort();
		let firstS = first.filter(unique);
		let secondS = second.filter(unique);
		let third = firstS.concat(secondS);
		third.sort();
		let thirdS = third.filter(unique);
		if(firstS.length+secondS.length==thirdS.length)
			dist = thirdS.length;
		else
		{
			if(firstS.length>secondS.length)
			{
				dist = thirdS.length-secondS.length;
			}
			else
			{
				dist = thirdS.length-firstS.length;
			}
		}
	}
	}
	else
	{
		let x = new Error("InvalidType");
		throw x;
	}
	return dist;
}


module.exports.distance = distance